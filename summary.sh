#!/bin/bash

mkdir -p reports

docker run --rm \
    -v `pwd`/templates:/templates \
    -v `pwd`/src/results/:/results \
    -e GIT_HASH="`git rev-parse HEAD`" \
    -e STAMP="`date "+%Y/%m/%d %H-%M-%S"`" \
    hairyhenderson/gomplate -d data=/results/intent_report.json -f /templates/summary.tmpl | tee reports/summary.md
